# MovieeQuest

## Overview
Welcome to MovieeQuest, a sophisticated web application developed with React. MovieeQuest is designed to offer a seamless and user-friendly experience for movie enthusiasts, enabling them to explore and discover films effortlessly.

## Getting Started
To begin using MovieeQuest:
1. Register for an account. If you already have an account, simply sign in.
Note: Before SignIn/SignUp any page that you click will reflect to SignUp page only. so please signUp/signIn first.
2. After SignUp, you'll be navigated to the "Search" section to find movies using relevant keywords.
3. Give the movie name and hit "search" button, the movies which are matched with the keywords given will be listed below. you can also see more details of that movie by clicking on "view more details" button.
4. You can also view and edit your profile details in the "Profile" tab after signing up or logging in.

## Project Structure
- *App.js*: Manages the routing for all components and pages within the application.
- *firebase.js*: Handles database connections and user authentication.
- *Components and Pages*: Contains all the individual pages and components used throughout the application.

## Running the Application
To run MovieeQuest locally:
1. Execute npm start to launch the project in development mode.
2. Use npm run build to build the project for production.

---

Thank you for using MovieeQuest! We hope you enjoy discovering new movies with our application.