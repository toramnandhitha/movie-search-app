import React from 'react';
import Navbar from './components/Navbar';
import './App.css';
import Home from './components/pages/Home';
import SignupMain from './components/pages/SignupMain';
import LoginMain from './components/pages/LoginMain';
import Profile from './components/pages/Profiles';
import Search from './components/Search';
//import Login from './components/pages/Login';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth, db, logout } from "./firebase";
import { useEffect, useState} from 'react';
import {collection, query, where, onSnapshot} from 'firebase/firestore';

function App() {

  const [user, loading] = useAuthState(auth);
  const authh = !(!user);
  const email = user?.email;
  const [state, setState] = useState(0);
  return (
      <>
  
        <Router>
          <Navbar />
          <Routes>
            <Route path='/' element={<Home />} />
            <Route path='/home' element={<Home />} />
            <Route path='/signupmain' element={<SignupMain />} />
            <Route path='/loginmain' element={<LoginMain />} />
            <Route path='/search' element={<Search />} />
            <Route path='/profile' element={<Profile />} />
          </Routes>
        </Router>
      </>
    );
}

export default App;
