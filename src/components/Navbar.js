import React, { useState, useEffect } from 'react';
import { Button } from 'react-bootstrap';
import { auth, logout } from "../firebase";
import { useAuthState } from 'react-firebase-hooks/auth';
import { Link, useNavigate } from 'react-router-dom';
import './Navbar.css';

function Navbar() {
  const [user] = useAuthState(auth);
  const authh = Boolean(user);
  const [click, setClick] = useState(false);
  const [button, setButton] = useState(true);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  let navigate = useNavigate();
  const routeChange = () => {
    let path = '/signupmain';
    navigate(path);
  }

  const showButton = () => {
    if (window.innerWidth <= 960) {
      setButton(false);
    } else {
      setButton(true);
    }
  };

  useEffect(() => {
    showButton();
  }, []);

  window.addEventListener('resize', showButton);

  return (
    <>
      <nav className='navbar'>
        <div className='navbar-container'>
          <Link to='/' className='navbar-logo' onClick={closeMobileMenu}>
            @MovieeQuest
          </Link>
          <div className='menu-icon' onClick={handleClick}>
            <i className={click ? 'fas fa-times' : 'fas fa-bars'} />
          </div>
          <ul className={click ? 'nav-menu active' : 'nav-menu'}>
            <li className='nav-item'>
              <Link to='/home' className='nav-links' onClick={closeMobileMenu}>
                Home
              </Link>
            </li>
            <li className='nav-item'>
              <Link to='/profile' className='nav-links' onClick={closeMobileMenu}>
                Profile
              </Link>
            </li>
          </ul>
          <ul className='nav-btn'>
            {authh ? (
              <li>{button && <Button onClick={logout} variant="light" size='md'>LOG OUT</Button>}</li>
            ) : (
              <li>{button && <Button onClick={routeChange} variant="light" size='md'>SIGN UP</Button>}</li>
            )}
          </ul>
        </div>
      </nav>
    </>
  );
}

export default Navbar;