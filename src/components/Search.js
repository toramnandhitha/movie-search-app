import React, { useState } from 'react';
import styled from 'styled-components';
import Modal from 'react-modal';

const API_KEY = 'e5094277';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px;
  background-color: #f5f5f5;
  min-height: 100vh;
`;

const Title = styled.h2`
  color: #333;
  margin-bottom: 20px;
`;

const Form = styled.form`
  display: flex;
  justify-content: center;
  margin-bottom: 20px;
`;

const Input = styled.input`
  padding: 10px;
  font-size: 16px;
  border: 1px solid #ccc;
  border-radius: 4px;
  width: 300px;
  margin-right: 10px;
`;

const Button = styled.button`
  padding: 10px 20px;
  font-size: 16px;
  color: #fff;
  background-color: #007bff;
  border: none;
  border-radius: 4px;
  cursor: pointer;

  &:hover {
    background-color: #0056b3;
  }
`;

const MoviesGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
  gap: 20px;
  width: 100%;
  max-width: 1000px;
`;

const MovieCard = styled.div`
  background-color: #fff;
  border: 1px solid #ddd;
  border-radius: 8px;
  overflow: hidden;
  box-shadow: 0 2px 8px rgba(0, 0, 0, 0.1);
  text-align: center;
  padding: 10px;
`;

const MovieTitle = styled.h3`
  font-size: 18px;
  color: #333;
  margin: 10px 0;
`;

const MovieYear = styled.p`
  font-size: 14px;
  color: #666;
  margin-bottom: 10px;
`;

const MoviePoster = styled.img`
  width: 100%;
  height: auto;
`;

const MoreDetailsButton = styled.button`
  padding: 8px 16px;
  font-size: 14px;
  color: #fff;
  background-color: #28a745;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  margin-top: 10px;

  &:hover {
    background-color: #218838;
  }
`;

const MovieDetails = styled.div`
  padding: 20px;
  text-align: left;
`;

const buttonStyle = {
  right: '10px',
  bottom: '10px',
  position: 'fixed',
  padding: '10px 20px',
  fontSize: '18px',
  background: 'rgb(141 188 217)',
  borderRadius: '5px',
  cursor: 'pointer',
};

const customStyles = {
  content: {
    top: '55%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    overflow: 'hidden',
  },
};

export default function Search() {
  const [query, setQuery] = useState('');
  const [movies, setMovies] = useState([]);
  const [selectedMovie, setSelectedMovie] = useState(null);

  const searchMovies = async (e) => {
    e.preventDefault();
    const url = `https://www.omdbapi.com/?s=${query}&apikey=${API_KEY}`;
    try {
      const response = await fetch(url);
      const data = await response.json();
      setMovies(data.Search || []);
    } catch (error) {
      console.error('Error fetching the movies:', error);
    }
  };

  const fetchMovieDetails = async (imdbID) => {
    const url = `https://www.omdbapi.com/?i=${imdbID}&apikey=${API_KEY}`;
    try {
      const response = await fetch(url);
      const data = await response.json();
      setSelectedMovie(data);
    } catch (error) {
      console.error('Error fetching movie details:', error);
    }
  };

  const handleMovieClick = (imdbID) => {
    fetchMovieDetails(imdbID);
  };

  const closeModal = () => {
    setSelectedMovie(null);
  };

  return (
    <Container>
      <Title>Search Movies</Title>
      <Form onSubmit={searchMovies}>
        <Input
          type="text"
          value={query}
          onChange={(e) => setQuery(e.target.value)}
          placeholder="Search for a movie"
        />
        <Button type="submit">Search</Button>
      </Form>
      <MoviesGrid>
        {movies.map((movie) => (
          <MovieCard key={movie.imdbID}>
            <MoviePoster src={movie.Poster} alt={movie.Title} />
            <MovieTitle>{movie.Title}</MovieTitle>
            <MovieYear>{movie.Year}</MovieYear>
            <MoreDetailsButton onClick={() => handleMovieClick(movie.imdbID)}>
              More Details
            </MoreDetailsButton>
          </MovieCard>
        ))}
      </MoviesGrid>
      {selectedMovie && (
        <Modal
          isOpen={!!selectedMovie}
          onRequestClose={closeModal}
          style={customStyles}
          contentLabel="Movie Details"
        >
          <MovieDetails>
            <h2>{selectedMovie.Title}</h2>
            <p>Year: {selectedMovie.Year}</p>
            <p>Rated: {selectedMovie.Rated}</p>
            <p>Runtime: {selectedMovie.Runtime}</p>
            <p>Director: {selectedMovie.Director}</p>
            <p>Actors: {selectedMovie.Actors}</p>
            <p>Plot: {selectedMovie.Plot}</p>
            <p>Language: {selectedMovie.Language}</p>
            <p>Country: {selectedMovie.Country}</p>
          </MovieDetails>
          <button style={buttonStyle} onClick={closeModal}>Close</button>
        </Modal>
      )}
    </Container>
  );
}