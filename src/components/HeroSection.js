import React from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { useSpring, animated } from 'react-spring';
import { useAuthState } from 'react-firebase-hooks/auth';
import { useEffect } from 'react';
import { Button } from 'react-bootstrap';
import { auth } from "../firebase";
import './HeroSection.css';
import './AnimatedLine.css'

function HeroSection() {
  const navigate = useNavigate();
  const [user] = useAuthState(auth);
  const authh = Boolean(user);
  useEffect(() => {
    document.body.style.overflow = 'hidden';
    return () => {
      document.body.style.overflow = 'auto';
    };
  }, []);

  const fadeIn = useSpring({
    opacity: 1,
    from: { opacity: 0 },
    config: { duration: 1000 },
  });

  const slideIn = useSpring({
    transform: 'translateY(0)',
    from: { transform: 'translateY(-100px)' },
    config: { duration: 1000 },
  });

  const routeChange = () => {
    if (authh) {
      let path = '/search';
      navigate(path);
    } else {
      let path = '/signupmain';
      navigate(path);
    }
  };

  return (
    <animated.div className="hero-container" style={fadeIn}>
      <animated.h1 className="vibrant-text" style={slideIn}>Discover Your Favorite Movies</animated.h1>
      <animated.p className="line1" style={slideIn}><b>Welcome to Our MovieeQuest Platform!!</b></animated.p>
      <div className='animated-line'></div>
      <div className="hero-btns">
        <Button
          onClick={routeChange}
          className="btns"
          variant="light"
          size="lg"
        >
          <b>GET STARTED</b>
        </Button>
      </div>
    </animated.div>
  );
}

export default HeroSection;