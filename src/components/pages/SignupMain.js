import { Container } from "react-bootstrap";
//import { Routes, Route } from "react-router-dom";
import "../../App.css";
//import LoginHome from "./LoginHome";
//import Login from "./Login";
import ClientSignup from "./Signup";
//import ProtectedRoute from "./ProtectedRoute";
import { UserAuthContextProvider } from "./UserAuthContext";

function SignupMain() {
  return (
    <div>
      
    <Container style={{ width: "400px" }}>
          <UserAuthContextProvider>
              <ClientSignup />
          </UserAuthContextProvider>
    </Container>
    </div>
  );
}

export default SignupMain;
