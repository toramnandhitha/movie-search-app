import React, { useState, useEffect } from 'react';
import { useAuthState } from "react-firebase-hooks/auth";
import { useNavigate } from "react-router-dom";
import { auth, db } from "../../firebase";
import { query, collection, where, updateDoc, doc, onSnapshot } from "firebase/firestore";
import Button from 'react-bootstrap/Button';
import './Profile.css';
import { Box } from "@mui/material";

const Profiles = () => {
  const [user, loading, error] = useAuthState(auth);
  const email = user?.email;
  const [name, setName] = useState("");
  const [state, setState] = useState("");
  const [phone, setPhone] = useState("");
  const [newName, setNewName] = useState("");
  const [newPhone, setNewPhone] = useState("");
  const [newState, setNewState] = useState("");
  const [docId, setDocId] = useState("");
  const [isToggled, setIsToggled] = useState(false);
  const [editMode, setEditMode] = useState(false);

  const navigate = useNavigate();

  useEffect(() => {
    if (loading) return;
    if (!user) return navigate("/signupmain");

    const collectionRef = collection(db, 'users');
    const q = query(collectionRef, where('email', '==', email));
    onSnapshot(q, (querySnapshot) => {
      querySnapshot.forEach((doc) => {
        setName(doc.data().name);
        setPhone(doc.data().phone);
        setState(doc.data().state);
        setDocId(doc.id);
      });
    });
  }, [user, loading, navigate, email]);

  const handleFormSubmit = async (event) => {
    event.preventDefault();
    const userRef = doc(db, state === 2001 ? 'users' : 'workers', docId);
    await updateDoc(userRef, {
      name: newName,
      phone: newPhone
    }).then(() => {
      console.log("User updated successfully");
      setName(newName);
      setPhone(newPhone);
      setIsToggled(false);
      setEditMode(false);
    }).catch((error) => {
      console.error("Error updating user: ", error);
    });
  }

  return (
    <div className="profile-container">
      {editMode ? (
        <form onSubmit={handleFormSubmit}>
          <label>
            Name:
            <input type="text" value={newName} onChange={(e) => setNewName(e.target.value)} />
          </label>
          <label>
            Phone:
            <input type="text" value={newPhone} onChange={(e) => setNewPhone(e.target.value)} />
          </label>
          <button type="submit">Save</button>
          <button type="button" onClick={() => {
            setIsToggled(false);
            setEditMode(false);
          }}>Cancel</button>
        </form>
      ) : (
        <div>
          <div className="profile-header">
            <h3>Hello, {name}</h3>
            <Button variant="outline-primary" size="large" onClick={() => {
              setIsToggled(true);
              setEditMode(true);
            }}>Edit</Button>
          </div>
          <div className="profile-details">
            <h3>
              <b>Email: </b>{email}<br />
              <b>Phone No.: </b>{phone}<br />
            </h3>
          </div>
        </div>
      )}
    </div>
  );
}

export default Profiles;